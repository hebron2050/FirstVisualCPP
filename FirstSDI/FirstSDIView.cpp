// FirstSDIView.cpp : implementation of the CFirstSDIView class
//

#include "stdafx.h"
#include "FirstSDI.h"

#include "FirstSDIDoc.h"
#include "FirstSDIView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFirstSDIView

IMPLEMENT_DYNCREATE(CFirstSDIView, CView)

BEGIN_MESSAGE_MAP(CFirstSDIView, CView)
	//{{AFX_MSG_MAP(CFirstSDIView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFirstSDIView construction/destruction

CFirstSDIView::CFirstSDIView()
{
	// TODO: add construction code here

}

CFirstSDIView::~CFirstSDIView()
{
}

BOOL CFirstSDIView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CFirstSDIView drawing

void CFirstSDIView::OnDraw(CDC* pDC)
{
	CFirstSDIDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CFirstSDIView printing

BOOL CFirstSDIView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CFirstSDIView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CFirstSDIView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CFirstSDIView diagnostics

#ifdef _DEBUG
void CFirstSDIView::AssertValid() const
{
	CView::AssertValid();
}

void CFirstSDIView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CFirstSDIDoc* CFirstSDIView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CFirstSDIDoc)));
	return (CFirstSDIDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFirstSDIView message handlers
