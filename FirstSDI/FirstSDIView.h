// FirstSDIView.h : interface of the CFirstSDIView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FIRSTSDIVIEW_H__2F7F46AA_7BF5_4DED_ADDA_54303CB93D8F__INCLUDED_)
#define AFX_FIRSTSDIVIEW_H__2F7F46AA_7BF5_4DED_ADDA_54303CB93D8F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CFirstSDIView : public CView
{
protected: // create from serialization only
	CFirstSDIView();
	DECLARE_DYNCREATE(CFirstSDIView)

// Attributes
public:
	CFirstSDIDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFirstSDIView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFirstSDIView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFirstSDIView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in FirstSDIView.cpp
inline CFirstSDIDoc* CFirstSDIView::GetDocument()
   { return (CFirstSDIDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIRSTSDIVIEW_H__2F7F46AA_7BF5_4DED_ADDA_54303CB93D8F__INCLUDED_)
