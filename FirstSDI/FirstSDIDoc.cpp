// FirstSDIDoc.cpp : implementation of the CFirstSDIDoc class
//

#include "stdafx.h"
#include "FirstSDI.h"

#include "FirstSDIDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFirstSDIDoc

IMPLEMENT_DYNCREATE(CFirstSDIDoc, CDocument)

BEGIN_MESSAGE_MAP(CFirstSDIDoc, CDocument)
	//{{AFX_MSG_MAP(CFirstSDIDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFirstSDIDoc construction/destruction

CFirstSDIDoc::CFirstSDIDoc()
{
	// TODO: add one-time construction code here

}

CFirstSDIDoc::~CFirstSDIDoc()
{
}

BOOL CFirstSDIDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CFirstSDIDoc serialization

void CFirstSDIDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CFirstSDIDoc diagnostics

#ifdef _DEBUG
void CFirstSDIDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CFirstSDIDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFirstSDIDoc commands
