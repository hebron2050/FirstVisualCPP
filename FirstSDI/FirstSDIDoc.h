// FirstSDIDoc.h : interface of the CFirstSDIDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FIRSTSDIDOC_H__1639C27E_7BB2_4836_BC22_0D79249C12A1__INCLUDED_)
#define AFX_FIRSTSDIDOC_H__1639C27E_7BB2_4836_BC22_0D79249C12A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CFirstSDIDoc : public CDocument
{
protected: // create from serialization only
	CFirstSDIDoc();
	DECLARE_DYNCREATE(CFirstSDIDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFirstSDIDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFirstSDIDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFirstSDIDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIRSTSDIDOC_H__1639C27E_7BB2_4836_BC22_0D79249C12A1__INCLUDED_)
