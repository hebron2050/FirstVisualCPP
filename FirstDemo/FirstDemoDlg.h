// FirstDemoDlg.h : header file
//

#if !defined(AFX_FIRSTDEMODLG_H__89B7F510_D863_4F9F_B87A_711B3351A431__INCLUDED_)
#define AFX_FIRSTDEMODLG_H__89B7F510_D863_4F9F_B87A_711B3351A431__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CFirstDemoDlg dialog

class CFirstDemoDlg : public CDialog
{
// Construction
public:
	CFirstDemoDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CFirstDemoDlg)
	enum { IDD = IDD_FIRSTDEMO_DIALOG };
	CString	m_strFirst;
	CString	m_strSecond;
	CString	m_strShow;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFirstDemoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CFirstDemoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBUTTONShow();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIRSTDEMODLG_H__89B7F510_D863_4F9F_B87A_711B3351A431__INCLUDED_)
