; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CFirstDemoDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "FirstDemo.h"

ClassCount=3
Class1=CFirstDemoApp
Class2=CFirstDemoDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_FIRSTDEMO_DIALOG

[CLS:CFirstDemoApp]
Type=0
HeaderFile=FirstDemo.h
ImplementationFile=FirstDemo.cpp
Filter=N

[CLS:CFirstDemoDlg]
Type=0
HeaderFile=FirstDemoDlg.h
ImplementationFile=FirstDemoDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CFirstDemoDlg

[CLS:CAboutDlg]
Type=0
HeaderFile=FirstDemoDlg.h
ImplementationFile=FirstDemoDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_FIRSTDEMO_DIALOG]
Type=1
Class=CFirstDemoDlg
ControlCount=9
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC_First,static,1342308352
Control5=IDC_EDIT_First,edit,1350631552
Control6=IDC_STATIC_Second,static,1342308352
Control7=IDC_EDIT_Second,edit,1350631552
Control8=IDC_BUTTON_Show,button,1342242816
Control9=IDC_EDIT_Show,edit,1350633600

