// FirstDemo.h : main header file for the FIRSTDEMO application
//

#if !defined(AFX_FIRSTDEMO_H__09122E1B_0A59_4857_92F4_192C475ADC06__INCLUDED_)
#define AFX_FIRSTDEMO_H__09122E1B_0A59_4857_92F4_192C475ADC06__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CFirstDemoApp:
// See FirstDemo.cpp for the implementation of this class
//

class CFirstDemoApp : public CWinApp
{
public:
	CFirstDemoApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFirstDemoApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CFirstDemoApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIRSTDEMO_H__09122E1B_0A59_4857_92F4_192C475ADC06__INCLUDED_)
