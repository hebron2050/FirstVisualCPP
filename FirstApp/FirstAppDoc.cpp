// FirstAppDoc.cpp : implementation of the CFirstAppDoc class
//

#include "stdafx.h"
#include "FirstApp.h"

#include "FirstAppDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFirstAppDoc

IMPLEMENT_DYNCREATE(CFirstAppDoc, CDocument)

BEGIN_MESSAGE_MAP(CFirstAppDoc, CDocument)
	//{{AFX_MSG_MAP(CFirstAppDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFirstAppDoc construction/destruction

CFirstAppDoc::CFirstAppDoc()
{
	// TODO: add one-time construction code here

}

CFirstAppDoc::~CFirstAppDoc()
{
}

BOOL CFirstAppDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CFirstAppDoc serialization

void CFirstAppDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CFirstAppDoc diagnostics

#ifdef _DEBUG
void CFirstAppDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CFirstAppDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFirstAppDoc commands
