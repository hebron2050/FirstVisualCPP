// FirstAppDoc.h : interface of the CFirstAppDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FIRSTAPPDOC_H__A274A666_6FE2_45E9_9302_C11A8BDFB9FA__INCLUDED_)
#define AFX_FIRSTAPPDOC_H__A274A666_6FE2_45E9_9302_C11A8BDFB9FA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CFirstAppDoc : public CDocument
{
protected: // create from serialization only
	CFirstAppDoc();
	DECLARE_DYNCREATE(CFirstAppDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFirstAppDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFirstAppDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFirstAppDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIRSTAPPDOC_H__A274A666_6FE2_45E9_9302_C11A8BDFB9FA__INCLUDED_)
