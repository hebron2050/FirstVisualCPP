// FirstAppView.h : interface of the CFirstAppView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FIRSTAPPVIEW_H__59BFF13A_2946_48D4_98C0_69BD37CF6E7C__INCLUDED_)
#define AFX_FIRSTAPPVIEW_H__59BFF13A_2946_48D4_98C0_69BD37CF6E7C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CFirstAppView : public CView
{
protected: // create from serialization only
	CFirstAppView();
	DECLARE_DYNCREATE(CFirstAppView)

// Attributes
public:
	CFirstAppDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFirstAppView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFirstAppView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFirstAppView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in FirstAppView.cpp
inline CFirstAppDoc* CFirstAppView::GetDocument()
   { return (CFirstAppDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIRSTAPPVIEW_H__59BFF13A_2946_48D4_98C0_69BD37CF6E7C__INCLUDED_)
