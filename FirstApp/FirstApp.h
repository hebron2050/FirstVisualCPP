// FirstApp.h : main header file for the FIRSTAPP application
//

#if !defined(AFX_FIRSTAPP_H__EA006CA3_65D9_4BCF_B5C2_C922A0FCF8FE__INCLUDED_)
#define AFX_FIRSTAPP_H__EA006CA3_65D9_4BCF_B5C2_C922A0FCF8FE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CFirstAppApp:
// See FirstApp.cpp for the implementation of this class
//

class CFirstAppApp : public CWinApp
{
public:
	CFirstAppApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFirstAppApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CFirstAppApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIRSTAPP_H__EA006CA3_65D9_4BCF_B5C2_C922A0FCF8FE__INCLUDED_)
