// FirstAppView.cpp : implementation of the CFirstAppView class
//

#include "stdafx.h"
#include "FirstApp.h"

#include "FirstAppDoc.h"
#include "FirstAppView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFirstAppView

IMPLEMENT_DYNCREATE(CFirstAppView, CView)

BEGIN_MESSAGE_MAP(CFirstAppView, CView)
	//{{AFX_MSG_MAP(CFirstAppView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFirstAppView construction/destruction

CFirstAppView::CFirstAppView()
{
	// TODO: add construction code here

}

CFirstAppView::~CFirstAppView()
{
}

BOOL CFirstAppView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CFirstAppView drawing

void CFirstAppView::OnDraw(CDC* pDC)
{
	CFirstAppDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CFirstAppView printing

BOOL CFirstAppView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CFirstAppView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CFirstAppView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CFirstAppView diagnostics

#ifdef _DEBUG
void CFirstAppView::AssertValid() const
{
	CView::AssertValid();
}

void CFirstAppView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CFirstAppDoc* CFirstAppView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CFirstAppDoc)));
	return (CFirstAppDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFirstAppView message handlers
