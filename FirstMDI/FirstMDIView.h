// FirstMDIView.h : interface of the CFirstMDIView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FIRSTMDIVIEW_H__B55AB6D1_E1DD_42A1_A034_686FF48A6500__INCLUDED_)
#define AFX_FIRSTMDIVIEW_H__B55AB6D1_E1DD_42A1_A034_686FF48A6500__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CFirstMDIView : public CView
{
protected: // create from serialization only
	CFirstMDIView();
	DECLARE_DYNCREATE(CFirstMDIView)

// Attributes
public:
	CFirstMDIDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFirstMDIView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFirstMDIView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFirstMDIView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in FirstMDIView.cpp
inline CFirstMDIDoc* CFirstMDIView::GetDocument()
   { return (CFirstMDIDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIRSTMDIVIEW_H__B55AB6D1_E1DD_42A1_A034_686FF48A6500__INCLUDED_)
