// FirstMDIView.cpp : implementation of the CFirstMDIView class
//

#include "stdafx.h"
#include "FirstMDI.h"

#include "FirstMDIDoc.h"
#include "FirstMDIView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFirstMDIView

IMPLEMENT_DYNCREATE(CFirstMDIView, CView)

BEGIN_MESSAGE_MAP(CFirstMDIView, CView)
	//{{AFX_MSG_MAP(CFirstMDIView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFirstMDIView construction/destruction

CFirstMDIView::CFirstMDIView()
{
	// TODO: add construction code here

}

CFirstMDIView::~CFirstMDIView()
{
}

BOOL CFirstMDIView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CFirstMDIView drawing

void CFirstMDIView::OnDraw(CDC* pDC)
{
	CFirstMDIDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CFirstMDIView printing

BOOL CFirstMDIView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CFirstMDIView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CFirstMDIView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CFirstMDIView diagnostics

#ifdef _DEBUG
void CFirstMDIView::AssertValid() const
{
	CView::AssertValid();
}

void CFirstMDIView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CFirstMDIDoc* CFirstMDIView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CFirstMDIDoc)));
	return (CFirstMDIDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFirstMDIView message handlers
