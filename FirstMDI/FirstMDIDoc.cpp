// FirstMDIDoc.cpp : implementation of the CFirstMDIDoc class
//

#include "stdafx.h"
#include "FirstMDI.h"

#include "FirstMDIDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFirstMDIDoc

IMPLEMENT_DYNCREATE(CFirstMDIDoc, CDocument)

BEGIN_MESSAGE_MAP(CFirstMDIDoc, CDocument)
	//{{AFX_MSG_MAP(CFirstMDIDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFirstMDIDoc construction/destruction

CFirstMDIDoc::CFirstMDIDoc()
{
	// TODO: add one-time construction code here

}

CFirstMDIDoc::~CFirstMDIDoc()
{
}

BOOL CFirstMDIDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CFirstMDIDoc serialization

void CFirstMDIDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CFirstMDIDoc diagnostics

#ifdef _DEBUG
void CFirstMDIDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CFirstMDIDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFirstMDIDoc commands
