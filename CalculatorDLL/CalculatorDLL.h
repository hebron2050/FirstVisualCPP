// CalculatorDLL.h : main header file for the CALCULATORDLL DLL
//

#if !defined(AFX_CALCULATORDLL_H__7BFEA41E_EED3_4A61_AA3F_887A44C291D6__INCLUDED_)
#define AFX_CALCULATORDLL_H__7BFEA41E_EED3_4A61_AA3F_887A44C291D6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCalculatorDLLApp
// See CalculatorDLL.cpp for the implementation of this class
//

class CCalculatorDLLApp : public CWinApp
{
public:
	CCalculatorDLLApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCalculatorDLLApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCalculatorDLLApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALCULATORDLL_H__7BFEA41E_EED3_4A61_AA3F_887A44C291D6__INCLUDED_)
